<?php

namespace App\Http\Middleware;

use App\Models\User;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class CheckToken {
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next) {
        $user = User::whereRememberToken($request->header('token'))->first();

        if ($user) {
            $request->user_id = $user->id;
            return $next($request);
        }

        abort(403);
    }
}
